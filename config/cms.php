<?php

return [
    'theme' => [
        'folder' => 'themes',
        'active' => 'default'
    ],

    'templates' => [
        'Page' => SundaySim\Templates\PageTemplates::class
    ]
];
