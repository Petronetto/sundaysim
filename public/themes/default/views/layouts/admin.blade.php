<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ theme('css/backend.css') }}">
    <script src="{{ theme('js/all.js') }}" charset="utf-8"></script>
    <script src="{{ URL::to('/themes/default/assets/vendor/tinymce/tinymce.min.js') }}" charset="utf-8"></script>
    <script src="" charset="utf-8"></script>
    <title>@yield('title') &ndash; The Sunday Sim</title>
  </head>
  <body>
      <nav class="navbar navbar-static-top navbar-inverse">
      	<div class="container">
      		<div class="navbar-header"><a href="/" class="navbar-brand">The Sunday Sim</a></div>
      		<ul class="nav navbar-nav">
      			<li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
      			<li><a href="{{ route('admin.users.index') }}">Users</a></li>
      			<li><a href="{{ route('admin.pages.index') }}">Pages</a></li>
      			<li><a href="{{ route('admin.blog.index') }}">Blog Posts</a></li>
      		</ul>
      		<ul class="nav navbar-nav navbar-right">
      			<li><span class="navbar-text">Hello, {{ $admin->name }}</span></li>
      			<li><a href="{{ route('auth.logout') }}">Logout</a></li>
      		</ul>
      	</div>
      </nav>

      <div class="container">
      	<div class="row">
      		<div class="col-md-12">
      			<h3>@yield('title')</h3>
                @if($errors->any())
                    <div class="alert alert-danger">
                        <strong>We found some errors!</strong>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if($status)
                    <div class="alert alert-info">
                        {{ $status }}
                    </div>
                @endif

      			@yield('content')
      		</div>
      	</div>
      </div>
  </body>
</html>
