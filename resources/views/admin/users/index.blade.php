@extends('layouts.admin')

@section('title', 'Users')

@section('content')
    <a href="{{ route('admin.users.create') }}"><button class="btn btn-primary">Create New User</button></a>
    <table class="table table-hover ">
    	<thead>
    		<tr>
    			<th>Name</th>
    			<th>E-mail</th>
    			<th>Edit</th>
    			<th>Delete</th>
    		</tr>
    	</thead>
    <tbody>
        @foreach($users as $user)
            <tr>
                <td>
                    <a href="{{ route('admin.users.edit', $user->id) }}">{{ $user->name }}</a>
                </td>
                <td>{{ $user->email }}</td>
                <td>
                    <a href="{{ route('admin.users.edit', $user->id) }}">
                        <span class="glyphicon glyphicon-edit"></span>
                    </a>
                </td>
                <td>
                    <a href="{{ route('admin.users.confirm', $user->id) }}">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
    </table>
    {!! $users->render() !!}
@endsection
