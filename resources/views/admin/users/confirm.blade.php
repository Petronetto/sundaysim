@extends('layouts.admin')

@section('title', 'Deleting ' . $user->name)

@section('content')
    {!! Form::open(['method' => 'delete', 'route' => ['admin.users.destroy', $user->id]]) !!}
        <div class="alert alert-danger">
            <strong>Warning!</strong> You are about to delete a user. This action can not be undone.
             Are you sure you want to continue?
        </div>
        {!! Form::submit('Yes, delete this user', ['class' => 'btn btn-danger']) !!}
        <a href="{{ route('admin.users.index') }}" class="btn btn-success">
            <strong>No, get me out here.</strong>
        </a>
    {!! Form::close() !!}
@endsection
