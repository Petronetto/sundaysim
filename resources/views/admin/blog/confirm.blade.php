@extends('layouts.admin')

@section('title', 'Deleting ' . $post->title)

@section('content')
    {!! Form::open(['method' => 'delete', 'route' => ['admin.blog.destroy', $post->id]]) !!}
        <div class="alert alert-danger">
            <strong>Warning!</strong> You are about to delete a post. This action can not be undone.
             Are you sure you want to continue?
        </div>
        {!! Form::submit('Yes, delete this post', ['class' => 'btn btn-danger']) !!}
        <a href="{{ route('admin.pages.index') }}" class="btn btn-success">
            <strong>No, get me out here.</strong>
        </a>
    {!! Form::close() !!}
@endsection
