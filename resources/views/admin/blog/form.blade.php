@extends('layouts.admin')

@section('title', $post->exists ? 'Editing '.$post->title : 'Create New Blog Post')

@section('content')

{!! Form::model($post, [
    'method' => $post->exists ? 'put' : 'post',
    'route'  => $post->exists ? ['admin.blog.update', $post->id] : ['admin.blog.store']
]) !!}

<div class="form-group">
    {!! Form::label('title') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('slug') !!}
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group row">
    <div class="col-md-12">
        {!! Form::label('published_at') !!}
    </div>
    <div class="col-md-4">
        {!! Form::text('published_at', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('excerpt') !!}
    {!! Form::textarea('excerpt', null, ['class' => 'form-control', 'rows' => '5']) !!}
</div>

<div class="form-group">
    {!! Form::label('body') !!}
    {!! Form::textarea('body', null, ['class' => 'form-control', 'id' => 'editor']) !!}
</div>

{!! Form::submit($post->exists ? 'Save Post' : 'Create New Post', ['class' => 'btn btn-primary'])!!}

{!! Form::close() !!}

<script>
    $('[name=published_at]').datetimepicker({
        allowInputToggle: true,
        format: 'YYYY-MM-DD HH:mm:ss',
        showClear: true,
        defaultDate: '{{ old('published_at', $post->published_at) }}'
    });

    $('[name=title]').on('blur', function() {
        var slugElement = $('[name=slug]');

        if (slugElement.val()) {
            return;
        }

        slugElement.val(
            this.value.toLowerCase()
            .replace(/[^a-z0-9-]+/g, '-')
            .replace(/^-+|-+$/g, '')
        );
    });


     var editor_config = {
        path_absolute : "{{ URL::to('/') }}/",
        selector: '#editor',
        language: 'pt_BR',
        height: 500,
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern imagetools"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }
            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no"
            });
        }
    };
    tinymce.init(editor_config);
</script>

@endsection
