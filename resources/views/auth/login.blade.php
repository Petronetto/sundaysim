@extends('layouts.auth')

@section('title', 'Login')

@section('heading', 'Welcome, please login.')

@section('content')
    {!! Form::open() !!}

    <div class="form-group">
        {!! Form::label('email') !!}
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('password') !!}
        {!! Form::password('password', ['class' => 'form-control']) !!}
    </div>

    <div class="checkbox">
        <label for="remember">
            {!! Form::checkbox('remember', null, false) !!}
            Remember me
        </label>
    </div>

    {!! Form::submit('Login', ['class' => 'btn btn-primary btn-block']) !!}

    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('auth.password.email') }}" class="small">Forgot password?</a>
        </div>
    </div>

    {!! Form::close() !!}
@endsection
