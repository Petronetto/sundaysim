<?php

use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->truncate();
        DB::table('posts')->insert([
            [
                'author_id' => 1,
                'title' => 'Example Post One',
                'slug' => 'example-post-one',
                'body' => 'This is an example post!',
                'published_at' => date('Y-m-d H:i:s', strtotime('now'))
            ],
            [
                'author_id' => 1,
                'title' => 'Example Post Two',
                'slug' => 'example-post-two',
                'body' => 'This is an another example post!',
                'published_at' => date('Y-m-d H:i:s', strtotime('+ 3 weeks'))
            ],
            [
                'author_id' => 1,
                'title' => 'Example Post Three',
                'slug' => 'example-post-three',
                'body' => 'This is an another example post!',
                'published_at' => null
            ]
        ]);
    }
}
