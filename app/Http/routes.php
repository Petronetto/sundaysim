<?php

Route::controller('auth/password', 'Auth\PasswordController', [
   'getEmail'  => 'auth.password.email',
   'getReset'  => 'auth.password.reset'
]);

Route::controller('auth', 'Auth\AuthController', [
   'getLogin'  => 'auth.login',
   'getLogout' => 'auth.logout'
]);

Route::get('admin/users/{users}/confirm', [
    'as' => 'admin.users.confirm',
    'uses' => 'Admin\UsersController@confirm'
]);
Route::resource('admin/users', 'Admin\UsersController', ['except' => ['show']]);

Route::get('admin/pages/{pages}/confirm', [
    'as' => 'admin.pages.confirm',
    'uses' => 'Admin\PagesController@confirm'
]);
Route::resource('admin/pages', 'Admin\PagesController', ['except' => ['show']]);

Route::get('admin/blog/{blog}/confirm', [
    'as' => 'admin.blog.confirm',
    'uses' => 'Admin\BlogController@confirm'
]);
Route::resource('admin/blog', 'Admin\BlogController');

Route::get('admin/dashboard', ['as' => 'admin.dashboard', 'uses' => 'Admin\DashboardController@index']);

Route::get('/', ['middleware' => 'auth' ,function () {
    return view('welcome');
}]);
