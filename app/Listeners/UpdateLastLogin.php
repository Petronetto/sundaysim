<?php

namespace SundaySim\Listeners;

use Carbon\Carbon;

/**
 * Update the column "last_login_at" in User class
 */
class UpdateLastLogin
{

    function handle($user, $remember)
    {
        $user->last_login_at = Carbon::now();

        $user->save();
    }
}
