<?php

namespace SundaySim\View\Composers;

use Illuminate\View\View;
use SundaySim\Page;

class Injectpages
{
    public function __construct(Page $pages)
    {
        $this->pages = $pages;
    }

    public function compose(View $view)
    {
        $pages = $this->pages->all()->toHierarchy();

        $view->with('pages', $pages);
    }
}
